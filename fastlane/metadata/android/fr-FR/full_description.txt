Jeu du démineur

Ce jeu du démineur, simple et classique, est à destination des enfants (mais pas seulement).

Si vous saver lire les chiffres, vous savez jouer !

Avec un choix volontairement limité dans les options, chacun peut découvrir et apprendre comment jouer et comment adapter le jeu à ses envies.

Il est possible de choisir :
#   la difficulté du jeu (nombre de mines) : débutant, facile, moyen, difficile
#   le taille de la grille : 10x10 (très facile, pour petits débutants), 15x15 (facile), 20x20 (classique)

Les mines sont placées après le premier coup, on ne peut pas marcher sur une mine à son premier pas.
