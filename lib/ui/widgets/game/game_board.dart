import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:minehunter/cubit/activity/activity_cubit.dart';
import 'package:minehunter/models/activity/activity.dart';
import 'package:minehunter/models/types.dart';
import 'package:minehunter/ui/widgets/game/cell.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final Board board = currentActivity.board;
        final Color borderColor = currentActivity.reportMode ? Colors.blue : Colors.black;

        return Container(
          margin: const EdgeInsets.all(2),
          padding: const EdgeInsets.all(2),
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: borderColor,
                  borderRadius: BorderRadius.circular(2),
                  border: Border.all(
                    color: borderColor,
                    width: 2,
                  ),
                ),
                child: Table(
                  defaultColumnWidth: const IntrinsicColumnWidth(),
                  children: [
                    for (int row = 0; row < currentActivity.sizeVertical; row++)
                      TableRow(
                        children: [
                          for (int col = 0; col < currentActivity.sizeHorizontal; col++)
                            Column(
                              children: [
                                CellWidget(cell: board[row][col], row: row, col: col)
                              ],
                            ),
                        ],
                      ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
