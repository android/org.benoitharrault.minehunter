import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:minehunter/config/application_config.dart';

import 'package:minehunter/cubit/activity/activity_cubit.dart';
import 'package:minehunter/models/activity/cell.dart';
import 'package:minehunter/models/activity/activity.dart';
import 'package:minehunter/utils/board_animate.dart';

class CellWidget extends StatelessWidget {
  const CellWidget({
    super.key,
    required this.cell,
    required this.row,
    required this.col,
  });

  final Cell cell;
  final int row;
  final int col;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;
        final ActivityCubit activityCubit = BlocProvider.of<ActivityCubit>(context);

        String imageAsset = getImageAssetName(
          currentActivity: currentActivity,
        );

        return Container(
          decoration: BoxDecoration(
            color: getBackgroundColor(currentActivity),
            border: getCellBorders(
              currentActivity: currentActivity,
              row: row,
              col: col,
            ),
          ),
          child: GestureDetector(
            child: AnimatedSwitcher(
              duration: const Duration(milliseconds: 100),
              transitionBuilder: (Widget child, Animation<double> animation) {
                return ScaleTransition(scale: animation, child: child);
              },
              child: Image(
                image: AssetImage(imageAsset),
                fit: BoxFit.fill,
                key: ValueKey<int>(imageAsset.hashCode),
              ),
            ),
            onTap: () {
              // Set mines on board after first player try
              if (!currentActivity.isBoardMined) {
                activityCubit.addMines(forbiddenRow: row, forbiddenCol: col);
              }

              if (!(currentActivity.gameWin || currentActivity.gameFail)) {
                if (currentActivity.reportMode) {
                  activityCubit.reportCell(
                    row: row,
                    col: col,
                  );
                } else {
                  activityCubit.walkOnCell(
                    row: row,
                    col: col,
                  );
                }
                if (activityCubit.checkGameIsFinished()) {
                  activityCubit.updateReportMode(false);
                  BoardAnimate.startAnimation(
                    activityCubit: activityCubit,
                    currentActivity: currentActivity,
                    animationType: currentActivity.gameWin ? 'win' : 'fail',
                  );
                }
              }
            },
          ),
        );
      },
    );
  }

  /*
  * Compute image asset name, from skin and cell value/state
  */
  String getImageAssetName({
    required Activity currentActivity,
  }) {
    final String skin =
        currentActivity.activitySettings.get(ApplicationConfig.parameterCodeSkin);

    String imageAsset = 'assets/skins/${skin}_tile_unknown.png';

    bool showSolution = currentActivity.gameWin || currentActivity.gameFail;
    if (!showSolution) {
      // Running game
      if (cell.isExplored) {
        if (cell.isMined) {
          // Boom
          imageAsset = 'assets/skins/${skin}_tile_mine.png';
        } else {
          // Show mines count around
          imageAsset = 'assets/skins/${skin}_tile_${cell.minesCountAround}.png';
        }
      } else {
        if (cell.isMarked) {
          // Danger!
          imageAsset = 'assets/skins/${skin}_tile_flag.png';
        }
      }
    } else {
      // Finished game
      if (cell.isMined) {
        if (cell.isExploded) {
          // Mine exploded
          imageAsset = 'assets/skins/${skin}_tile_mine.png';
        } else {
          // Mine not found
          imageAsset = 'assets/skins/${skin}_tile_mine_not_found.png';
        }
      } else {
        // Show all mines counts
        imageAsset = 'assets/skins/${skin}_tile_${cell.minesCountAround}.png';
      }
    }

    return imageAsset;
  }

  // Compute cell background color, from cell state
  Color getBackgroundColor(Activity currentActivity) {
    if (currentActivity.gameWin) {
      return cell.isAnimated ? Colors.green.shade400 : Colors.green.shade500;
    }
    if (currentActivity.gameFail) {
      return cell.isAnimated ? Colors.pink.shade200 : Colors.pink.shade400;
    }

    return cell.isAnimated ? Colors.white : Colors.grey.shade200;
  }

  // Compute cell borders, from board size and cell state
  Border getCellBorders({
    required Activity currentActivity,
    required int row,
    required int col,
  }) {
    Color cellBorderColor = Colors.grey.shade500;
    double cellBorderWidth = 4;

    // Reduce cell border width on big boards
    int boardSize = currentActivity.sizeHorizontal;
    if (boardSize > 8) {
      cellBorderWidth = 2;
      if (boardSize > 10) {
        cellBorderWidth = 1;
      }
    }

    if (currentActivity.gameWin) {
      cellBorderColor = Colors.green.shade700;
    } else if (currentActivity.gameFail) {
      cellBorderColor = Colors.pink.shade300;
    }

    Border borders = Border.all(
      color: cellBorderColor,
      width: cellBorderWidth,
    );

    return borders;
  }
}
