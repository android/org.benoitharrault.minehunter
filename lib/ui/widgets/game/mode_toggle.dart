import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:minehunter/config/application_config.dart';

import 'package:minehunter/cubit/activity/activity_cubit.dart';
import 'package:minehunter/models/activity/activity.dart';
import 'package:minehunter/ui/widgets/game/mode_button_toggle.dart';
import 'package:minehunter/ui/widgets/game/mode_indicator_report.dart';
import 'package:minehunter/ui/widgets/game/mode_indicator_walk.dart';

class ToggleGameMode extends StatelessWidget {
  const ToggleGameMode({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final String skin =
            currentActivity.activitySettings.get(ApplicationConfig.parameterCodeSkin);

        final Image paddingBlock = Image(
          image: AssetImage('assets/skins/${skin}_empty.png'),
          fit: BoxFit.fill,
        );

        return Table(
          defaultColumnWidth: const IntrinsicColumnWidth(),
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: [
            TableRow(
              children: [
                TableCell(child: paddingBlock),
                const TableCell(child: GameModeIndicatorWalk()),
                const TableCell(child: ToggleGameModeButton()),
                const TableCell(child: GameModeIndicatorReport()),
                TableCell(child: paddingBlock),
              ],
            ),
          ],
        );
      },
    );
  }
}
