import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:minehunter/config/application_config.dart';

import 'package:minehunter/cubit/activity/activity_cubit.dart';
import 'package:minehunter/models/activity/activity.dart';

class GameModeIndicatorWalk extends StatelessWidget {
  const GameModeIndicatorWalk({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final String skin =
            currentActivity.activitySettings.get(ApplicationConfig.parameterCodeSkin);
        final String reportModeSuffix = currentActivity.reportMode ? 'off' : 'on';
        final Color buttonColor = currentActivity.reportMode ? Colors.grey : Colors.amber;

        return StyledButton(
          color: buttonColor,
          child: Image(
            image: AssetImage('assets/skins/${skin}_indicator_walk_$reportModeSuffix.png'),
            fit: BoxFit.fill,
          ),
          onPressed: () {
            BlocProvider.of<ActivityCubit>(context).updateReportMode(false);
          },
        );
      },
    );
  }
}
