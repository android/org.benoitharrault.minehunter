import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:minehunter/config/application_config.dart';

import 'package:minehunter/cubit/activity/activity_cubit.dart';
import 'package:minehunter/models/activity/activity.dart';

class TopIndicator extends StatelessWidget {
  const TopIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final String skin =
            currentActivity.activitySettings.get(ApplicationConfig.parameterCodeSkin);

        final int flaggedCellsCount = currentActivity.countFlaggedCells();
        final int minesCount = currentActivity.minesCount;
        const double blockSize = 30;

        final blockWidth = MediaQuery.of(context).size.width / 4;

        final Image flagIconBlock = Image(
          image: AssetImage('assets/skins/${skin}_tile_flag.png'),
          fit: BoxFit.fill,
          height: blockSize,
          width: blockSize,
        );
        final Image mineIconBlock = Image(
          image: AssetImage('assets/skins/${skin}_tile_mine.png'),
          fit: BoxFit.fill,
          height: blockSize,
          width: blockSize,
        );
        final Text markedMinesCountBlock = Text(
          flaggedCellsCount.toString(),
          style: TextStyle(
            fontSize: blockSize,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).colorScheme.primary,
          ),
        );
        final Text placedMinesCountBlock = Text(
          minesCount.toString(),
          style: TextStyle(
            fontSize: blockSize,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).colorScheme.primary,
          ),
        );

        final Widget flagsWidget = StyledContainer(
          borderWidth: 8,
          child: SizedBox(
            width: blockWidth,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                markedMinesCountBlock,
                flagIconBlock,
              ],
            ),
          ),
        );

        final Widget minesWidget = StyledContainer(
          borderWidth: 8,
          child: SizedBox(
            width: blockWidth,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                mineIconBlock,
                placedMinesCountBlock,
              ],
            ),
          ),
        );

        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                flagsWidget,
                const SizedBox(width: blockSize * 2),
                minesWidget,
              ],
            )
          ],
        );
      },
    );
  }
}
