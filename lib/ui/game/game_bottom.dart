import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:minehunter/cubit/activity/activity_cubit.dart';
import 'package:minehunter/models/activity/activity.dart';
import 'package:minehunter/ui/widgets/game/mode_toggle.dart';

class GameBottomWidget extends StatelessWidget {
  const GameBottomWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return !currentActivity.isFinished ? const ToggleGameMode() : const SizedBox.shrink();
      },
    );
  }
}
