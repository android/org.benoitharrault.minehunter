import 'package:flutter/material.dart';

import 'package:minehunter/ui/widgets/indicators/indicator_top.dart';

class GameTopWidget extends StatelessWidget {
  const GameTopWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const TopIndicator();
  }
}
