import 'dart:async';
import 'dart:math';

import 'package:minehunter/cubit/activity/activity_cubit.dart';
import 'package:minehunter/models/activity/activity.dart';
import 'package:minehunter/models/types.dart';

class BoardAnimate {
  // Start game animation: blinking tiles
  static AnimatedBoardSequence createStartGameAnimationPatterns({
    required Activity currentActivity,
  }) {
    final AnimatedBoardSequence patterns = [];

    const int patternsCount = 4;
    final int sizeHorizontal = currentActivity.sizeHorizontal;
    final int sizeVertical = currentActivity.sizeVertical;

    for (int patternIndex = 0; patternIndex < patternsCount; patternIndex++) {
      final AnimatedBoard pattern = [];
      for (int row = 0; row < sizeVertical; row++) {
        final List<bool> patternRow = [];
        for (int col = 0; col < sizeHorizontal; col++) {
          patternRow.add(((patternIndex + row + col) % 2 == 0));
        }
        pattern.add(patternRow);
      }
      patterns.add(pattern);
    }

    return patterns;
  }

  // Failed game animation: explosions blowing from exploded mines
  static AnimatedBoardSequence createExplosionAnimationPatterns({
    required Activity currentActivity,
  }) {
    final AnimatedBoardSequence patterns = [];

    final int sizeHorizontal = currentActivity.sizeHorizontal;
    final int sizeVertical = currentActivity.sizeVertical;

    final List<List<int>> explodedMines = [];
    for (int row = 0; row < sizeVertical; row++) {
      for (int col = 0; col < sizeHorizontal; col++) {
        if (currentActivity.board[row][col].isExploded) {
          explodedMines.add([row, col]);
        }
      }
    }
    if (explodedMines.isEmpty) {
      explodedMines.add([(sizeVertical / 2).floor(), (sizeHorizontal / 2).floor()]);
    }

    final int patternsCount = max(sizeHorizontal, sizeVertical);

    for (int patternIndex = 0; patternIndex < patternsCount; patternIndex++) {
      final AnimatedBoard pattern = [];
      for (int row = 0; row < sizeVertical; row++) {
        final List<bool> patternRow = [];
        for (int col = 0; col < sizeHorizontal; col++) {
          bool isHilighted = false;
          for (int mineIndex = 0; mineIndex < explodedMines.length; mineIndex++) {
            double distance = sqrt(pow((explodedMines[mineIndex][0] - row), 2) +
                pow((explodedMines[mineIndex][1] - col), 2));
            isHilighted = isHilighted || ((patternIndex + distance) % 4 < 2);
          }
          patternRow.add(isHilighted);
        }
        pattern.add(patternRow);
      }
      patterns.add(pattern);
    }

    return patterns;
  }

  // Win game animation: rotating rays from center
  static AnimatedBoardSequence createWinGameAnimationPatterns({
    required Activity currentActivity,
  }) {
    final AnimatedBoardSequence patterns = [];

    const int patternsCount = 20;
    final int sizeHorizontal = currentActivity.sizeHorizontal;
    final int sizeVertical = currentActivity.sizeVertical;

    for (int patternIndex = 0; patternIndex < patternsCount; patternIndex++) {
      AnimatedBoard pattern = [];
      for (int row = 0; row < sizeVertical; row++) {
        List<bool> patternRow = [];
        for (int col = 0; col < sizeHorizontal; col++) {
          double angle = 2 * atan2((sizeVertical / 2) - col, (sizeHorizontal / 2) - row);
          patternRow.add((angle + patternIndex / 3) % 2 < 1);
        }
        pattern.add(patternRow);
      }
      patterns.add(pattern);
    }

    return patterns;
  }

  // Default multi-purpose animation: sliding stripes, from top left to right bottom
  static AnimatedBoardSequence createDefaultAnimationPatterns({
    required Activity currentActivity,
  }) {
    final AnimatedBoardSequence patterns = [];

    const int patternsCount = 16;
    final int sizeHorizontal = currentActivity.sizeHorizontal;
    final int sizeVertical = currentActivity.sizeVertical;

    for (int patternIndex = 0; patternIndex < patternsCount; patternIndex++) {
      final AnimatedBoard pattern = [];
      for (int row = 0; row < sizeVertical; row++) {
        final List<bool> patternRow = [];
        for (int col = 0; col < sizeHorizontal; col++) {
          patternRow.add(((patternIndex + row + col) % 4 == 0));
        }
        pattern.add(patternRow);
      }
      patterns.add(pattern);
    }

    return patterns;
  }

  static void startAnimation({
    required ActivityCubit activityCubit,
    required Activity currentActivity,
    required String animationType,
  }) {
    AnimatedBoardSequence patterns = [];

    switch (animationType) {
      case 'start':
        patterns = createStartGameAnimationPatterns(
          currentActivity: currentActivity,
        );
        break;
      case 'win':
        patterns = createWinGameAnimationPatterns(
          currentActivity: currentActivity,
        );
        break;
      case 'fail':
        patterns = createExplosionAnimationPatterns(
          currentActivity: currentActivity,
        );
        break;
      default:
        patterns = createDefaultAnimationPatterns(
          currentActivity: currentActivity,
        );
    }

    int patternIndex = patterns.length;

    activityCubit.updateAnimationInProgress(true);

    const interval = Duration(milliseconds: 200);
    Timer.periodic(
      interval,
      (Timer timer) {
        if (patternIndex == 0) {
          timer.cancel();
          activityCubit.resetAnimatedBackground();
          activityCubit.updateAnimationInProgress(false);
        } else {
          patternIndex--;
          activityCubit.setAnimatedBackground(patterns[patternIndex]);
        }
      },
    );
  }
}
