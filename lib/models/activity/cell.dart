class Cell {
  Cell(
    this.isMined,
  );

  bool isMined = false;
  bool isExplored = false;
  bool isMarked = false;
  bool isExploded = false;
  int minesCountAround = 0;

  bool isAnimated = false;

  @override
  String toString() {
    return '$Cell(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'isMined': isMined,
      'isExplored': isExplored,
      'isMarked': isMarked,
      'isExploded': isExploded,
      'minesCountAround': minesCountAround,
      'isAnimated': isAnimated,
    };
  }
}
