import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:minehunter/config/application_config.dart';

import 'package:minehunter/models/activity/cell.dart';
import 'package:minehunter/models/types.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.isFinished = false,
    this.animationInProgress = false,

    // Base data
    required this.board,
    required this.sizeHorizontal,
    required this.sizeVertical,
    this.isBoardMined = false,

    // Game data
    this.minesCount = 0,
    this.reportMode = false,
    this.gameWin = false,
    this.gameFail = false,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool isFinished;
  bool animationInProgress;

  // Base data
  Board board;
  final int sizeHorizontal;
  final int sizeVertical;
  bool isBoardMined;

  // Game data
  int minesCount;
  bool reportMode;
  bool gameWin;
  bool gameFail;

  factory Activity.createEmpty() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      board: [],
      sizeHorizontal: 0,
      sizeVertical: 0,
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    final int sizeHorizontal = int.parse(
        newActivitySettings.get(ApplicationConfig.parameterCodeBoardSize).split('x')[0]);
    final int sizeVertical = int.parse(
        newActivitySettings.get(ApplicationConfig.parameterCodeBoardSize).split('x')[1]);

    // Create empty board (it will be mined after first hit)
    final Board board = [];
    for (int rowIndex = 0; rowIndex < sizeVertical; rowIndex++) {
      final List<Cell> row = [];
      for (int colIndex = 0; colIndex < sizeHorizontal; colIndex++) {
        row.add(Cell(false));
      }
      board.add(row);
    }

    final int minesCountRatio = ApplicationConfig.minesCountRatios[
            newActivitySettings.get(ApplicationConfig.parameterCodeDifficultyLevel)] ??
        1;
    final int minesCount = ((sizeHorizontal * sizeVertical) * minesCountRatio / 100).round();
    printlog('Mines count: $minesCount');

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      // Base data
      board: board,
      sizeHorizontal: sizeHorizontal,
      sizeVertical: sizeVertical,
      // Game data
      minesCount: minesCount,
    );
  }

  void addMines({
    required int forbiddenRow,
    required int forbiddenCol,
  }) {
    // Shuffle cells to put random mines, except on currently selected one
    final List<List<int>> allowedCells = [];
    for (int row = 0; row < sizeVertical; row++) {
      for (int col = 0; col < sizeHorizontal; col++) {
        if (!((forbiddenRow == row) && (forbiddenCol == col))) {
          allowedCells.add([row, col]);
        }
      }
    }
    allowedCells.shuffle();

    // Put random mines on board
    for (int mineIndex = 0; mineIndex < minesCount; mineIndex++) {
      board[allowedCells[mineIndex][0]][allowedCells[mineIndex][1]].isMined = true;
    }

    // Compute all mines counts on cells
    for (int row = 0; row < sizeVertical; row++) {
      for (int col = 0; col < sizeHorizontal; col++) {
        board[row][col].minesCountAround = getMinesCountAround(row, col);
      }
    }

    printGrid();
  }

  int getMinesCountAround(int row, int col) {
    final int sizeHorizontal = board.length;
    final int sizeVertical = board[0].length;

    int minesCountAround = 0;
    for (int deltaRow = -1; deltaRow <= 1; deltaRow++) {
      for (int deltaCol = -1; deltaCol <= 1; deltaCol++) {
        if ((row + deltaRow >= 0 && row + deltaRow < sizeVertical) &&
            (col + deltaCol >= 0 && col + deltaCol < sizeHorizontal) &&
            (board[row + deltaRow][col + deltaCol].isMined)) {
          minesCountAround++;
        }
      }
    }

    return minesCountAround;
  }

  int countFlaggedCells() {
    int count = 0;

    final int sizeHorizontal = board.length;
    final int sizeVertical = board[0].length;
    for (int row = 0; row < sizeVertical; row++) {
      for (int col = 0; col < sizeHorizontal; col++) {
        if (board[row][col].isMarked == true) {
          count++;
        }
      }
    }

    return count;
  }

  bool get canBeResumed => isStarted && !isFinished;
  bool get gameWon => isRunning && isStarted && isFinished;

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    printlog('  Base data');
    printGrid();
    printlog('    sizeHorizontal: $sizeHorizontal');
    printlog('    sizeVertical: $sizeVertical');
    printlog('    isBoardMined: $isBoardMined');
    printlog('  Game data');
    printlog('    minesCount: $minesCount');
    printlog('    reportMode: $reportMode');
    printlog('    gameWin: $gameWin');
    printlog('    gameFail: $gameFail');
    printlog('');
  }

  void printGrid() {
    const String isMined = 'X';
    const String isSafe = '.';

    const String mineFound = '#';
    const String wrongMarkedCell = '0';
    const String exploredSafeCell = '.';
    const String unkownState = ' ';

    printlog('');
    String line = '--';
    for (int i = 0; i < board[0].length; i++) {
      line += '-';
    }
    printlog('$line  $line');
    for (int rowIndex = 0; rowIndex < board.length; rowIndex++) {
      String currentLine = '';
      String solvedLine = '';
      for (int colIndex = 0; colIndex < board[rowIndex].length; colIndex++) {
        solvedLine += board[rowIndex][colIndex].isMined ? isMined : isSafe;

        String cellString = unkownState;
        if (board[rowIndex][colIndex].isExplored) {
          cellString = exploredSafeCell;
        }
        if (board[rowIndex][colIndex].isMarked) {
          if (board[rowIndex][colIndex].isMined) {
            cellString = mineFound;
          } else {
            cellString = wrongMarkedCell;
          }
        }
        currentLine += cellString;
      }
      printlog('|$currentLine|  |$solvedLine|');
    }
    printlog('$line  $line');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      // Base data
      'board': board,
      'sizeHorizontal': sizeHorizontal,
      'sizeVertical': sizeVertical,
      'isBoardMined': isBoardMined,
      // Game data
      'minesCount': minesCount,
      'reportMode': reportMode,
      'gameWin': gameWin,
      'gameFail': gameFail,
    };
  }
}
