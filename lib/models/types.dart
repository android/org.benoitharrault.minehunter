import 'package:minehunter/models/activity/cell.dart';

typedef Board = List<List<Cell>>;
typedef AnimatedBoard = List<List<bool>>;
typedef AnimatedBoardSequence = List<AnimatedBoard>;
