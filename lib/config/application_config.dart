import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:minehunter/cubit/activity/activity_cubit.dart';

import 'package:minehunter/ui/pages/game.dart';

import 'package:minehunter/ui/parameters/parameter_painter_board_size.dart';
import 'package:minehunter/ui/parameters/parameter_painter_difficulty_level.dart';

class ApplicationConfig {
  // activity parameter: skin
  static const String parameterCodeSkin = 'global.skin';
  static const String skinValueDefault = 'default';

  // activity parameter: difficulty level
  static const String parameterCodeDifficultyLevel = 'level';
  static const String difficultyLevelValueEasy = 'easy';
  static const String difficultyLevelValueMedium = 'medium';
  static const String difficultyLevelValueHard = 'hard';
  static const String difficultyLevelValueNightmare = 'nightmare';

  // activity parameter: board size
  static const String parameterCodeBoardSize = 'size';
  static const String boardSizeValueSmall = '10x10';
  static const String boardSizeValueMedium = '15x15';
  static const String boardSizeValueLarge = '20x20';
  static const String boardSizeValueExtraLarge = '25x25';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Minehunter',
    activitySettings: [
      // skin
      ApplicationSettingsParameter(
        code: parameterCodeSkin,
        values: [
          ApplicationSettingsParameterItemValue(
            value: skinValueDefault,
            isDefault: true,
          ),
        ],
      ),

      // difficulty level
      ApplicationSettingsParameter(
        code: parameterCodeDifficultyLevel,
        values: [
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueEasy,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueMedium,
            color: Colors.orange,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueHard,
            color: Colors.red,
          ),
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueNightmare,
            color: Colors.purple,
          ),
        ],
        customPainter: (context, value) => ParameterPainterDifficultyLevel(
          context: context,
          value: value,
        ),
      ),

      // board size
      ApplicationSettingsParameter(
        code: parameterCodeBoardSize,
        values: [
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueSmall,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueMedium,
            color: Colors.orange,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueLarge,
            color: Colors.red,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueExtraLarge,
            color: Colors.purple,
          ),
        ],
        customPainter: (context, value) => ParameterPainterBoardSize(
          context: context,
          value: value,
        ),
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );

  // how many mines? (per 100 tiles)
  static const Map<String, int> minesCountRatios = {
    ApplicationConfig.difficultyLevelValueEasy: 5,
    ApplicationConfig.difficultyLevelValueMedium: 10,
    ApplicationConfig.difficultyLevelValueHard: 15,
    ApplicationConfig.difficultyLevelValueNightmare: 20,
  };
}
